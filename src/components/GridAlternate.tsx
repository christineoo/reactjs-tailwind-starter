import React from "react";

interface Props {
  imageTitle: string;
  reverse: boolean;
}

export const GridAlternate = ({ imageTitle, reverse }: Props) => {
  return (
    <div
      className={`${
        reverse ? `sm:flex-row-reverse` : ""
      } flex flex-wrap p-6 lg:px-20 items-center justify-center`}
    >
      <div className="w-5/6 sm:w-1/3">
        <p className="text-gray-900 font-semibold pb-4">
          Vestibulum sed enim ante
        </p>
        <span className="text-gray-600">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
          sed enim ante. Lorem ipsum dolor sit amet, consectetur adipiscing
          elit. Vestibulum sed enim ante.
        </span>
      </div>
      <div className="w-full p-10 sm:w-2/3 md:w-2/6">
        <img src={require(`../images/${imageTitle}.svg`)} alt={imageTitle} />
      </div>
    </div>
  );
};
