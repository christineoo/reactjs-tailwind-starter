import React from "react";
import { NavigationBar } from "./components/NavigationBar";
import { HeroWave } from "./components/HeroWave";
import { Footer } from "./components/Footer";
import { GridAlternate } from "./components/GridAlternate";

const App: React.FC = () => {
  return (
    <>
      <NavigationBar />
      <HeroWave />
      <GridAlternate imageTitle="undraw_good_doggy" reverse={false} />
      <GridAlternate imageTitle="undraw_dog_walking" reverse />
      <GridAlternate imageTitle="undraw_nature_fun" reverse={false} />
      <Footer />
    </>
  );
};

export default App;
