import React from "react";

export const HeroWave = () => {
  return (
    <>
      <div className="bg-teal-500 text-white flex flex-col items-center pl-4 md:pl-20 pt-20">
        <span className="text-3xl md:text-5xl">ReactJS with</span>
        <span className="text-3xl md:text-5xl">Tailwind CSS Starter Page</span>
        <br />
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#38B2AC"
          fillOpacity="1"
          d="M0,0L60,32C120,64,240,128,360,160C480,192,600,192,720,176C840,160,960,128,1080,144C1200,160,1320,224,1380,256L1440,288L1440,0L1380,0C1320,0,1200,0,1080,0C960,0,840,0,720,0C600,0,480,0,360,0C240,0,120,0,60,0L0,0Z"
        ></path>
      </svg>
    </>
  );
};
