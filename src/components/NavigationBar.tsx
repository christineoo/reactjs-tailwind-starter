import React from "react";
import useOpen from "../hooks/useOpen";
import logo from "../images/undraw_cautious_dog.svg"; // --> OFF

// Turn off the warning for the invalid href="#" <a /> tags
/* eslint jsx-a11y/anchor-is-valid: 0 */

export const NavigationBar = () => {
  const { isOpen, toggle } = useOpen();

  return (
    <header className="bg-teal-500 sm:flex sm:justify-between sm:items-center  md:px-20 sm:px-4">
      <div className="flex items-center justify-between px-4 py-3 sm:p-0">
        <div className="flex items-center">
          <img className="h-10" src={logo} alt="logo" />
          <span className="pl-4 text-white font-semibold">
            ReactJS and Tailwind CSS
          </span>
        </div>
        <div className="sm:hidden">
          <button
            type="button"
            className="block text-teal-200 hover:text-white focus:text-white focus:outline-none"
            onClick={() => toggle()}
          >
            <svg
              className="fill-current h-6 w-6"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Menu</title>
              {isOpen ? (
                <path
                  fillRule="evenodd"
                  d="M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"
                />
              ) : (
                <path
                  fillRule="evenodd"
                  d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"
                />
              )}
            </svg>
          </button>
        </div>
      </div>
      <div className={`${isOpen ? `block` : `hidden`} px-2 pt-2 pb-4 sm:flex`}>
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white md:py-2 focus:text-white"
          href="#"
        >
          Home
        </a>
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white sm:mt-0 sm:ml-2 md:py-2 focus:text-white"
          href="#"
        >
          About
        </a>
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white sm:mt-0 sm:ml-2 md:py-2 focus:text-white"
          href="#"
        >
          Contact
        </a>
      </div>
    </header>
  );
};
