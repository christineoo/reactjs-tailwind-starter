import React from "react";

// Turn off the warning for the invalid href="#" <a /> tags
/* eslint jsx-a11y/anchor-is-valid: 0 */

export const Footer = () => {
  return (
    <footer>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#38B2AC"
          fillOpacity="1.5"
          d="M0,160L120,144C240,128,480,96,720,101.3C960,107,1200,149,1320,170.7L1440,192L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"
        ></path>
      </svg>
      <div className="bg-teal-500 p-10 flex items-center justify-center">
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white md:py-2 focus:text-white"
          rel="noopener noreferrer"
          target="_blank"
          href="https://reactjs.org/"
        >
          ReactJS
        </a>
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white sm:mt-0 sm:ml-2 md:py-2 focus:text-white"
          rel="noopener noreferrer"
          target="_blank"
          href="https://tailwindcss.com/"
        >
          Tailwind CSS
        </a>
        <a
          className="block px-2 py-1 text-teal-200 hover:text-white sm:mt-0 sm:ml-2 md:py-2 focus:text-white"
          href="#"
        >
          Github
        </a>
      </div>
    </footer>
  );
};
